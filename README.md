# Python protobuf helloworld application
## Dev environment setup
1. Install gRPC library for Python
   1. Run `pip install grpcio`.
2. Install gRPC Tools library for Python
   1. Run `pip install grpcio-tools`.
## Build
1. Goto `protobuf-helloworld` project root folder.
2. Run `python -m grpc_tools.protoc --proto_path=protos --python_out=. --pyi_out=. --grpc_python_out=. protos\helloworld.proto`
## Run
1. Run `python greeter_server.py`
## Test
1. Run `python greeter_client.py`